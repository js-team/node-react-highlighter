# Installation
> `npm install --save @types/react-highlighter`

# Summary
This package contains type definitions for react-highlighter (https://github.com/helior/react-highlighter).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-highlighter.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/react-highlighter/index.d.ts)
````ts
// Type definitions for react-highlighter 0.3
// Project: https://github.com/helior/react-highlighter
// Definitions by: DefinitelyTyped <https://github.com/DefinitelyTyped>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped
// TypeScript Version: 2.8

/// <reference types="react" />

declare var Highlight: any;
export = Highlight;

````

### Additional Details
 * Last updated: Thu, 23 Dec 2021 23:35:37 GMT
 * Dependencies: [@types/react](https://npmjs.com/package/@types/react)
 * Global values: none

# Credits
These definitions were written by [DefinitelyTyped](https://github.com/DefinitelyTyped).
